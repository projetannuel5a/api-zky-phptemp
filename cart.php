<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

header('Access-Control-Allow-Credentials:true');
header('Access-Control-Allow-Headers:Content-Type, Accept');
header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Origin:*');
?>{
  "cartLineList":[
    {
      "product":{
        "id" : "8",
        "reference" : "03-2072",
        "category" : "cable",
        "subCategory" : "télévision",
        "name" : "Fiche TV mâle 9,5mm - haute qualitée",
        "additionalInfos" : "3.50 femelle blister",
        "factoryPrice" : 0.7,
        "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
      },
      "quantity": 3
    },
    {
      "product":{
        "id" : "1",
        "reference" : "03-2071",
        "category" : "cable",
        "subCategory" : "télévision",
        "name" : "Fiche TV mâle 9,5mm - haute qualitée",
        "additionalInfos" : "3.50 femelle blister",
        "factoryPrice" : 0.7,
        "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
      },
      "quantity": 7
    },
    {
      "product":{
        "id" : "5",
        "reference" : "03-2075",
        "category" : "cable",
        "subCategory" : "télévision",
        "name" : "Fiche TV mâle 9,5mm - haute qualitée",
        "additionalInfos" : "3.50 femelle blister",
        "factoryPrice" : 0.7,
        "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
      },
      "quantity": 1
    }
  ]
}