<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

header('Access-Control-Allow-Credentials:true');
header('Access-Control-Allow-Headers:Content-Type, Accept');
header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Origin:*');

if( strlen( $_GET["term"] ) < 5 ){
?>[{
  "id" : "1",
  "reference" : "03-2071",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
},
{
  "id" : "2",
  "reference" : "03-2072",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/4246/thumb__wishlistImageNormal/Tub_SelectPlus_850_9016_BT.jpeg"
},
{
  "id" : "3",
  "reference" : "03-2073",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
},
{
  "id" : "4",
  "reference" : "03-2074",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/4246/thumb__wishlistImageNormal/Tub_SelectPlus_850_9016_BT.jpeg"
},
{
  "id" : "5",
  "reference" : "03-2075",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
},
{
  "id" : "6",
  "reference" : "03-2076",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/4246/thumb__wishlistImageNormal/Tub_SelectPlus_850_9016_BT.jpeg"
},
{
  "id" : "7",
  "reference" : "03-2077",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
},
{
  "id" : "8",
  "reference" : "03-2072",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/4246/thumb__wishlistImageNormal/Tub_SelectPlus_850_9016_BT.jpeg"
}
]<?php
}
else if( strlen( $_GET["term"] ) < 10 ){
?>[{
  "id" : "1",
  "reference" : "03-2071",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
},
{
  "id" : "2",
  "reference" : "03-2072",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/4246/thumb__wishlistImageNormal/Tub_SelectPlus_850_9016_BT.jpeg"
},
{
  "id" : "3",
  "reference" : "03-2073",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
}
]<?php
}
else{
?>[{
  "id" : "1",
  "reference" : "03-2071",
  "category" : "cable",
  "subCategory" : "télévision",
  "name" : "Fiche TV mâle 9,5mm - haute qualitée",
  "additionalInfos" : "3.50 femelle blister",
  "factoryPrice" : 0.7,
  "image" : "http://wishlist1.zone-secure.net/website/var/tmp/image-thumbnails/0/3402/thumb__wishlistImageThumbnail/COMPRIBAND%20TRS%20-%20Boite%20debout.jpeg"
}
]<?php
}
?>